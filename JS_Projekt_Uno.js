var baseURL = "http://nowaunoweb.azurewebsites.net";
var game;
var topCard;
var players = {};
var currentPlayer;
var playerMap = {};
//Index vom Player im Array im Auge behalten im Fall von Reverse (zur Nutzung bei Draw2 und Draw4)
var movePlayerIndex = 1;
var playerIndex;
var wildColor = "";


$(document).ready(function () {
  //Hintergrund ausblenden
  $(".stackCards").hide();
  for(let i = 0; i<4; i++){
    $("#player"+i).hide();
    $("#hand"+i).hide();
  }
  //Modal öffnet sich und Spielernamen können eingegeben werden
  let validatePlayerNames = function(form) {
    let fields = $("input:text", form)
    let values = fields.map(function () {
        return this.value;
    }).filter(function () {
        return this.length > 0;
    }).get();
    return (new Set(values)).size == fields.length;
};

$('#playerNames').modal();


$('#playerNamesForm').on('keyup', function (evt) {
    console.log(evt);
    if (validatePlayerNames(evt.currentTarget)) {
        $('button', evt.currentTarget).prop('disabled', false);
    }
});

$('#playerNamesForm').on('submit', function (evt) {
    evt.preventDefault();

    let names = $('input:text').map(function() {
        return this.value;
    }).get();

    let request = $.ajax({
        url: baseURL + "/api/Game/Start",
        method: 'POST',
        data: JSON.stringify(names),
        contentType: 'application/json',
        dataType: 'json'
    });

    request.done(function (data) {
      
        console.log(data);

        game = data;
        gameId = game.Id;
        topCard = game.TopCard;
        players = game.Players;

        //initiale TopCard zeigen
        getTopCardFromServer("");
        let del = 500;
        for(let i = 0; i < 4; i++){          
          playerMap[players[i].Player] = $("#player"+i);
          $("#player"+i).append("<div> player: "+players[i].Player+" </div>");
          $("#player"+i).append("<div> &nbsp &nbsp &nbsp &nbsp</div>");
          $("#player"+i).append("<div id='score"+i+"'> score: "+players[i].Score+"</div>");
          $("#player"+i).delay(del*i).slideDown();
          
          //initiale Handkarten anzeigen
          showCards(players[i].Player, players[i].Cards);
          $("#hand"+i).delay(del*i).slideDown();

          //highlight und hover Klasse nur bei current player
          if(i === 0){
            currentPlayer = players[i].Player;
            activatePlayerHighlight(currentPlayer);
          }
        }
        
        $('#playerNames').modal('hide');
        //Spielfeld einblenden     
        $(".stackCards").delay(del*4).fadeIn();
    });
    
    request.fail(function (msg) {
        console.log("Error in request", msg);
    });

});
});

function getTopCardFromServer(wildColor){
    let request = $.ajax({
    url: baseURL + "/api/Game/TopCard/"+gameId,
    method: 'GET',
    contentType: 'application/json',
    dataType: 'json'
  });
  request.done(function (data) {
    $('#topCard').attr("src", 'cards/'+ data.Color+''+data.Value+'.png');
    if(wildColor === ""){
      $("#topCard").removeAttr("style");
      
    }
    else{
      //schöne Farben für den Rand einfügen, damit sie zu den Player Farben passen
      let wildColorCSS;
      if(wildColor === "Red"){
        wildColorCSS = "hsl(1, 77%, 58%)";
      }
      else if(wildColor === "Yellow"){
        wildColorCSS = "hsl(49, 98%, 60%)";
      }
      else if(wildColor === "Blue"){
        wildColorCSS = "hsl(208, 79%, 58%)";
      }
      else if(wildColor === "Green"){
        wildColorCSS = "hsl(123, 41%, 50%)";
      }
      $("#topCard").css("border", "5px solid "+wildColorCSS);
    }
    topCard = data;
  });
  request.fail(function (msg) {
    console.log("Error in request", msg);
});
}

function getCardsPerPlayer(playerName){
  let request = $.ajax({
    url: baseURL + "/api/Game/GetCards/"+gameId+"?playerName="+playerName,
    method: 'GET',
    contentType: 'application/json',
    dataType: 'json'
  });
  
  request.done(function (data) {
    for(let i = 0; i<4; i++){
      if(players[i].Player === playerName){
        players[i] = data;
        //first remove then update score
        $("#score"+i).remove();
        $("#player"+i).append("<div id='score"+i+"'> score: "+players[i].Score+"</div>");
        //show cards
        showCards(playerName, data.Cards);
      }
      
    }
  });
  request.fail(function (msg) {
    console.log("Error in request", msg);
});
}

//Karte anklicken und spielen
$(".cardsPlayer").on("click", function(evt1){
    let $playerCard = $(evt1.target).data('player');
    //überprüfen, ob die angeklickte Karte zum currentPlayer gehört
    if($playerCard != currentPlayer){
      return;
    }
    
    let $currentCard = $(evt1.target).data('card');
    if($currentCard.Color === "Black"){
      $("#ColorChange").modal('show');
      $(".colorChangeBox").on("click", function(evt2){
        wildColor = $(evt2.target).attr('id');
        //event handler wieder löschen! sonst hängen irgendwann zu viele dran und es funktioniert nicht mehr
        $(".colorChangeBox").off("click");
        $("#ColorChange").modal('hide');
        playCard(evt1, $currentCard.Value, $currentCard.Color, wildColor, $currentCard)      
      })      
    }
    else {
      playCard(evt1, $currentCard.Value, $currentCard.Color, wildColor, $currentCard);
    }     
})

//Karte vom Stapel abheben
$("#cardBack").on("click", function(){
  let request = $.ajax({
    url: baseURL + "/api/Game/DrawCard/"+gameId,
    method: 'PUT',
    contentType: 'application/json',
    dataType: 'json'
  });
  request.done(function (data) {
    //Karten beim Player neu laden
    getCardsPerPlayer(data.Player);
    currentPlayer = data.NextPlayer;
    activatePlayerHighlight(currentPlayer, wildColor);
  });
  request.fail(function (msg) {
    console.log("Error in request", msg);
});
})

function playCard(evt, value, color, wildColor, currentCard){
  let request = $.ajax({
    url: baseURL + "/api/Game/PlayCard/"+gameId+"?value="+value+"&color="+color+"&wildColor="+wildColor,
    method: 'PUT',
    contentType: 'application/json',
    dataType: 'json'
  });
  request.done(function (data) {
    if(data.hasOwnProperty("error")){
      //abbrechen, falls eine invalide Karte angeklickt wird
      return;
    }

    //currentPlayer updaten
    //falls reverse gespielt wird, muss die Richtung geändert werden
    if(currentCard.Text === "Reverse"){
      if(movePlayerIndex === -1){
        movePlayerIndex = 1;
      }
      else {
        movePlayerIndex = -1;
      }
    }
    //wenn Draw2 oder Draw4 gespielt wurde, muss man einen Schritt "zurück" und die Karten von dem entsprechenden Player updaten
    for(let i = 0; i<4; i++){
    if(players[i].Player === currentPlayer){
      playerIndex = i;
      players[i].Score = players[i].Score - currentCard.Score;
      //first remove then update score
     $("#score"+i).remove();
     $("#player"+i).append("<div id='score"+i+"'> score: "+players[i].Score+"</div>");
    }
  }
    if(currentCard.Text === "Draw2" || currentCard.Text === "Draw4"){      
      if(playerIndex === 0 && movePlayerIndex === -1){
        currentPlayer = players[3].Player;
      }
      else if(playerIndex === 3 && movePlayerIndex === 1){
        currentPlayer = players[0].Player;
      }
      else {
        currentPlayer = players[(playerIndex + movePlayerIndex)].Player;
      }
      
      //Karten aktualisieren     
      for(let i = 0; i<4; i++){
        if(players[i].Player === currentPlayer){
          playerIndex = i;
          //alle Karten für den Player wegschmeißen und alles neu einfügen
        $("#hand"+playerIndex).children().remove();          
        getCardsPerPlayer(players[playerIndex].Player);                        
        }
      }                
      }      
      //wenn alle Karten aktualisiert sind, kann der currentPlayer wieder an die Server Response angeglichen werden
    //bzw wenn weder Draw2 noch Draw4 gespielt wurde, kann es gleich so übernommen werden
    currentPlayer = data.Player;
    //topCard intern aktualisieren, damit hover ordentlich funktioniert
    topCard = currentCard;
    if(topCard.Color != "Black"){
      wildColor = "";
    }
    //hightlight für current player aktivieren
    activatePlayerHighlight(currentPlayer, wildColor);
     //gespielte Karte entfernen
      $(evt.target).remove();
      
      //falls ein Spieler gewonnen hat (sprich score ist 0)
      if(data.Score === 0){
        $(".col-12").fadeOut();
        for(let i = 0; i<4;i++){
          if(players[i].Score === 0){
            $("#winner").append("<h1>"+players[i].Player+" is the winner</h1>");
          }
          else {
            $("#scores").append("<p>Player: "+players[i].Player+"&nbsp &nbsp &nbsp &nbsp Score: "+players[i].Score+"</p>");
          } 
        }
      $("#winnerModal").modal('show');
      }

      //neue TopCard anzeigen
    getTopCardFromServer(wildColor);
         
    });
  request.fail(function (msg) {
    console.log("Error in request", msg);
});

}
//aktiviert die hightlight Klasse für Player Name und Score und stellt sicher, dass hover nur beim currentPlayer funktioniert
function activatePlayerHighlight(currentPlayer, wildColor){
  for(let i = 0; i<4; i++){
    if(players[i].Player === currentPlayer){
      $("#player"+i).addClass("highlight");
      $("#hand"+i).addClass("hand"+i+"_hover");
      for(let j = 0; j<players[i].Cards.length; j++){
        if(checkIfCardCanBePlayed(players[i].Cards[j], wildColor)){
          $("#hand"+i).children("#"+i+""+j+"").addClass("card_hover");
        }       
      }     
    }
    else{
      $("#player"+i).removeClass("highlight");
      $("#hand"+i).removeClass("hand"+i+"_hover");
      $("#hand"+i).children().removeClass("card_hover");
    }
  }
}

//Karten für einen Spieler anzeigen
function showCards(player, cards){
  for(let i = 0; i<4; i++){
    if(players[i].Player === player){
      playerIndex = i;
    }
  }
  $("#hand"+playerIndex).children().remove();
        for(let index = 0; index < cards.length; index++){
          let $card = $("<img src="+'cards/'+ cards[index].Color+''+cards[index].Value+'.png'+" class=card id="+playerIndex+""+index+">");
         $card.data('card', cards[index]);
         $card.data('player', players[playerIndex].Player);
        $("#hand"+playerIndex).append($card);
        }  
}

//check, ob Karte gespielt werden kann
function checkIfCardCanBePlayed(card, wildColor){
  if(topCard.Value === card.Value || topCard.Color === card.Color || card.Color === "Black"){
    return true; 
  }
  //falls die topCard ein Farbwechsler ist
  else if(topCard.Value > 12 && wildColor === card.Color){
      return true;   
  }
  return false;
}




